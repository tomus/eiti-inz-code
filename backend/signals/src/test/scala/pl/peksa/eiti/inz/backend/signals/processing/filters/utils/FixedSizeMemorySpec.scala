package pl.peksa.eiti.inz.backend.signals.processing.filters.utils

import org.scalatest.{Matchers, WordSpec}

class FixedSizeMemorySpec extends WordSpec with Matchers {

  "buffer" when {
    "empty" should {
      val emptyBuffer = new FixedSizeMemory[Int](3)
      "throw an exception on get" in {
        intercept[IndexOutOfBoundsException] {
          emptyBuffer.get(0)
        }
      }
      "add value" in {
        emptyBuffer.add(3)
      }
    }
    "has one value" should {
      val buffer = new FixedSizeMemory[Int](3)
      buffer.add(3)
      "get value by index 0" in {
        buffer.get(0)
      }
      "should throw exception for value with index 1" in {
        intercept[IndexOutOfBoundsException] {
          buffer.get(1)
        }
      }
    }
    "is full" should {
      val fullBuffer = new FixedSizeMemory[Int](3)
      fullBuffer.add(1)
      fullBuffer.add(3)
      fullBuffer.add(5)
      "return valid values for all indexes" in {
        fullBuffer.get(0) should be(5)
        fullBuffer.get(1) should be(3)
        fullBuffer.get(2) should be(1)
      }
      "accept new values" in {
        fullBuffer.add(8)
      }
      "fail to get value exceeding the size" in {
        intercept[IndexOutOfBoundsException] {
          fullBuffer.get(3)
        }
      }
    }
    "has size 3 and 4th value is added" should {
      val overfilledBuffer = new FixedSizeMemory[Int](3)
      overfilledBuffer.add(1)
      overfilledBuffer.add(3)
      overfilledBuffer.add(5)
      overfilledBuffer.add(8)
      "return valid values for all indexes" in {
        overfilledBuffer.get(0) should be(8)
        overfilledBuffer.get(1) should be(5)
        overfilledBuffer.get(2) should be(3)
      }
    }
  }

}
