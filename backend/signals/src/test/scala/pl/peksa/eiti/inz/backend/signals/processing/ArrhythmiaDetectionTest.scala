package pl.peksa.eiti.inz.backend.signals.processing

import java.io.StringWriter
import java.math.{RoundingMode, BigDecimal}

import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{FunSuite, Matchers}
import pl.peksa.eiti.inz.backend.signals.processing.filters.QrsDetection._
import pl.peksa.eiti.inz.backend.signals.processing.filters.RrInterval.{ArrhythmiaDetector, IntervalCalculator}
import pl.peksa.eiti.inz.backend.signals.processing.filters.{MovingWindowIntegrator, RrInterval}
import pl.peksa.eiti.inz.backend.signals.testing.{LatexTable, Row, ComparisonTest}

import scala.collection.mutable.ListBuffer

class ArrhythmiaDetectionTest extends FunSuite with Matchers with TableDrivenPropertyChecks with ComparisonTest {

  import pl.peksa.eiti.inz.backend.signals.testing.SignalCsvReader._

  //  val records = Table("recordName", "100", "101", /*"102", */"103", /*"104", */"105", "106")//, "107", "108", "109", "111", "112", "113", "114", "115", "116", "117", "118", "119", "121", "122", "123", "124", "200", "201", "202", "203", "205", "207", "208", "209", "210", "212", "213", /*"214", */"215", "217", "219", "220", "221", "222", "223", "228", "230", "231", "232", "233", "234")
    val records = Table("recordName", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "111", "112", "113", "114", "115", "116", "117", "118", "119", "121", "122", "123", "124", "200", "201", "202", "203", "205", "207", "208", "209", "210", "212", "213", /*"214",*/ "215", "217", "219", "220", "221", "222", "223", /*"228", */ "230", /*"231", */ "232", "233", "234")
//  val records = Table("recordName", "113")

  private[this] def calculateRSignal(recordName: String): List[Int] = {
    val inputSignals = readSignals(csvForRecord(recordName))
    val mliiSignal = inputSignals.find(s => s.name.contains("MLII"))

    mliiSignal.map { mlii =>
      mlii.samples.values
        .map(new LowPassFilter)
        .map(new HighPassFilter)
        .map(new QrsDifferentator)
        .map(new QrsSquare)
        .map(new RDetector)
        .map(new RFilter)
        .map(_.toDouble)
        .map(new IntervalCalculator)
        .map(new ArrhythmiaDetector)
        .toList
    }.getOrElse(List.empty[Int])
  }

  import pl.peksa.eiti.inz.backend.signals.testing.PhysioBankAnnotations._

  val searchWindowSize = 10 * 320

  val fp = new ListBuffer[Int]

  def doTest(recordName: String): Option[TestResult] = {
    val reference = readAnnotations(recordName).filter(_.meaning.isBeat).filterNot(_.meaning.isNormal)
    val calculatedResult = calculateRSignal(recordName)

    if (calculatedResult.nonEmpty && reference.nonEmpty) {
      val wasArrhythmiaFound = reference.map { annotation =>
        calculatedResult.near(annotation.sampleNo).exists(_ > 0)
      }
      val warArrhythmiaIncorrectlyFound = calculatedResult.iterator.map(new RFilter).zipWithIndex.map { case (sampleValue, sampleNo) =>
        val isArrhythmiaPoint = sampleValue > 0
        def isInReference = reference.exists(_.sampleNo isNear sampleNo)
        val incorrect = isArrhythmiaPoint && !isInReference
        if (incorrect) fp += sampleValue
        incorrect
      }
      val result = new TestResult(recordName, wasArrhythmiaFound.count(_ == true), reference.size, warArrhythmiaIncorrectlyFound.count(_ == true), "arrhythmia")
      val summary = reference.zip(wasArrhythmiaFound).groupBy(_._1.meaning).mapValues(x => s"t:${x.count(_._2)},f:${x.count(!_._2)}")
      println(result.toFormattedString + "\n" + summary)
      Some(result)
    } else {
      None
    }
  }

  test("detected R points are the same (within given distance)") {
    val results = records.toList.par.flatMap { recordName =>
      doTest(recordName).map(List(_)).getOrElse(List.empty)
    }
    val allFound = results.map(_.foundCount).sum
    val allReference = results.map(_.referenceCount).sum
    val allFalsePositives = results.map(_.falsePositiveCount).sum
    val allPercent = BigDecimal.valueOf(allFound).divide(BigDecimal.valueOf(allReference), 4, RoundingMode.HALF_UP)
    println(s"$allPercent, $allFalsePositives")

    val caption = "Wyniki wykrywania arytmii w ,,MIT--BIH Arrhythmia Database'' w porównaniu z oznaczeniami kardiologów"
    val header = new Row(List(
      "Nazwa nagrania",
      "Liczba wykrytych",
      "Liczba spodziewanych",
      "\\%",
      "Liczba fałszywie wykrytych"
    ))

    val rows = results.toList.map { result: TestResult =>
      new Row(List(
        result.recordName,
        result.foundCount,
        result.referenceCount,
        result.efficiency.multiply(BigDecimal.valueOf(100)).setScale(2),
        result.falsePositiveCount
      ))
    }

    val table = new LatexTable(caption, "|l||c|c|c|c|", header, "tab:result:arrhythmia", rows)
    val writer = new StringWriter()
    table.write(writer)
    println(writer.toString)

    fp.groupBy(x => x).foreach { case (kind: Int, elems: Iterable[_]) =>
        println(s"fp $kind, ${elems.size}")
    }
  }
}
