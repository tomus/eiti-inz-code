package pl.peksa.eiti.inz.backend.signals.testing

import org.scalatest.{FunSuite, Matchers}

class SignalTestHelpersTest extends FunSuite with Matchers {

  import pl.peksa.eiti.inz.backend.signals.testing.SignalTestHelpers._

  test("should count how many consecutive values are the same") {
    countConsecutiveSameValues(List(1, 1, 0, 0, 0, 1, 1, 0, 1, 1).map(_.toDouble).iterator).toList should equal(List(2, 3, 2, 1, 2))
  }
}
