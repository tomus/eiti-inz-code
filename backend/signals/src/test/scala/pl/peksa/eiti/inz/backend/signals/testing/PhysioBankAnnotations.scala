package pl.peksa.eiti.inz.backend.signals.testing

import java.util.Scanner
import java.util.regex.Pattern

import pl.peksa.eiti.inz.backend.signals.testing.SignalCsvReader._

object PhysioBankAnnotations {

  def readAnnotations(recordName: String, annotationName: String = "atr"): Iterable[Annotation] = {
    import scala.collection.JavaConversions._

    val scanner = new Scanner(csvForRecord(s"$recordName-$annotationName"))
    scanner.useDelimiter(Pattern.compile("\\n"))
    val reader = scanner.map(_.split(','))
    reader.map { values =>
      new Annotation(values(0).toInt, AnnotationMeaning.codeToMeaning(values(1)))
    }.toList
  }

  sealed abstract class AnnotationMeaning {
    val isNormal: Boolean
    val isBeat: Boolean
  }

  sealed abstract class AbnormalBeat extends AnnotationMeaning {
    override val isNormal: Boolean = false
    override val isBeat: Boolean = true
  }

  sealed abstract class NonBeatAnnotation extends AnnotationMeaning {
    override val isNormal: Boolean = true
    override val isBeat: Boolean = false
  }

  object AnnotationMeaning {
    def codeToMeaning(code: String): AnnotationMeaning = code match {
      case "N" => NormalBeat
      case "A" => AtrialPrematureBeat
      case "V" => PrematureVentricularContraction
      case "+" => RhythmChange
      case "~" => ChangeInSignalQuality
      case "|" => IsolatedQrsLikeArtifact
      case "!" => VentricularFlutterWave
      case "/" => PacedBeat
      case "\"" => CommentAnnotation
      case "[" => StartOfVentricularFlutterFibrillation
      case "]" => EndOfVentricularFlutterFibrillation
      case "a" => AberratedAtrialPrematureBeat
      case "e" => AtrialEscapeBeat
      case "E" => VentricularEscapeBeat
      case "f" => FusionOfPacedAndNormalBeat
      case "F" => FusionOfVentricularAndNormalBeat
      case "j" => NodalJunctionalEscapeBeat
      case "J" => NodalJunctionalPrematureBeat
      case "L" => LeftBundleBranchBlockBeat
      case "Q" => UnclassifiableBeat
      case "R" => RightBundleBranchBlockBeat
      case "S" => StSegmentChange
      case "x" => NonConductedPWave

    }
  }

  sealed abstract class NormalBeatLike extends AnnotationMeaning {
    override val isNormal: Boolean = true
    override val isBeat: Boolean = true
  }

  case object NormalBeat extends NormalBeatLike
  case object PacedBeat extends NormalBeatLike
  case object FusionOfPacedAndNormalBeat extends NormalBeatLike
  case object FusionOfVentricularAndNormalBeat extends NormalBeatLike
  case object LeftBundleBranchBlockBeat extends NormalBeatLike
  case object RightBundleBranchBlockBeat extends NormalBeatLike
  case object UnclassifiableBeat extends NormalBeatLike
  case object AtrialPrematureBeat extends AbnormalBeat
  case object PrematureVentricularContraction extends AbnormalBeat
  case object AberratedAtrialPrematureBeat extends AbnormalBeat
  case object AtrialEscapeBeat extends AbnormalBeat
  case object VentricularEscapeBeat extends AbnormalBeat
  case object NodalJunctionalEscapeBeat extends AbnormalBeat
  case object NodalJunctionalPrematureBeat extends AbnormalBeat
  case object RhythmChange extends NonBeatAnnotation
  case object ChangeInSignalQuality extends NonBeatAnnotation
  case object IsolatedQrsLikeArtifact extends NonBeatAnnotation
  case object VentricularFlutterWave extends NonBeatAnnotation
  case object CommentAnnotation extends NonBeatAnnotation
  case object StartOfVentricularFlutterFibrillation extends NonBeatAnnotation
  case object EndOfVentricularFlutterFibrillation extends NonBeatAnnotation
  case object StSegmentChange extends NonBeatAnnotation
  case object NonConductedPWave extends NonBeatAnnotation

  sealed case class Annotation(sampleNo: Int, meaning: AnnotationMeaning)

}
