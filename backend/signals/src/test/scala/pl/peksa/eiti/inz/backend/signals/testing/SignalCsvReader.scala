package pl.peksa.eiti.inz.backend.signals.testing

import java.io.{File, FileInputStream, InputStreamReader, Reader}
import java.time.ZonedDateTime
import java.util.Scanner
import java.util.regex.Pattern

import pl.peksa.eiti.inz.backend.signals.responses.{Frequency, SignalPart, SignalWithSamples}

import scala.collection.JavaConversions._

trait SignalCsvReader {
  def readSignals(csvFile: Reader): Set[SignalWithSamples] = {
    val scanner = new Scanner(csvFile)
    scanner.useDelimiter(Pattern.compile("\\n"))
    val reader = scanner.map(_.split(','))
//    val stopwatch = Stopwatch.createStarted()
//    val countingIterator = reader.zipWithIndex.map { x =>
//      val (line, lineNo) = x
//      if ((lineNo & 1023) == 0) {
//        println(s"$lineNo lines read in ${stopwatch.elapsed(TimeUnit.MILLISECONDS)} ms")
//      }
//      line
//    }
    val lines = reader
    val keys: Seq[(String, String)] = lines.next().zip(lines.next())
    keys.zipWithIndex.map { case ((signalName, unit), column) =>
      val samples = new SignalPart(ZonedDateTime.now(), new Frequency(360.0), lines.map(_(column).toDouble).toStream)
      new SignalWithSamples(column, "mV", signalName, samples)
    }.toSet
  }

  def csvForRecord(recordName: String): Reader = {
    val filename = "mit-bih" + File.separator + recordName + ".csv"
    val file = new File(filename)
    val inputStream = new FileInputStream(file)
    new InputStreamReader(inputStream)
  }

}

object SignalCsvReader extends SignalCsvReader