package pl.peksa.eiti.inz.backend.signals.testing

import java.io.StringWriter

import scala.collection.immutable.Seq

final case class LatexTable(caption: String, layout: String, header: Row, label: String, rows: Seq[Row]) {
  def valid = rows.forall(_.values.size == header.values.size)

  if (!valid) throw new IllegalArgumentException

  def write(writer: StringWriter): Unit = {
    writer.write(tableHead)
    rows.foreach { row => writer.write(row.toLatex); writer.write("\n")}
    writer.write(tableFoot)
  }

  def tableHead =
    s"""
       |\\begin{longtable}{$layout}
       |\\caption{$caption}\\label{$label} \\\\
       |\\hline
       |${header.toLatex}
       |\\hline
       |\\endfirsthead
       |\\hline
       |${header.toLatex}
       |\\hline
       |\\endhead
       |\\hline
       |\\endfoot
       |""".stripMargin

  def tableFoot =
    s"""
       |\\hline
       |\\end{longtable}
       |""".stripMargin
}

final case class Row(values: Seq[Any]) {
  def toLatex: String = (values.map(_.toString) mkString "\t&\t") + "\\\\"
}
