package pl.peksa.eiti.inz.backend.signals.testing

import java.math.{RoundingMode, BigDecimal}

trait ComparisonTest {

  val searchWindowSize: Int

  case class TestResult(recordName: String, foundCount: Int, referenceCount: Int, falsePositiveCount: Int, testName: String) {
    val efficiency = if (referenceCount > 0) BigDecimal.valueOf(foundCount).divide(BigDecimal.valueOf(referenceCount), 4, RoundingMode.HALF_UP) else null

    def toFormattedString = f"$testName - $recordName: $foundCount%04d of $referenceCount%04d - $efficiency, false positives: $falsePositiveCount"
  }

  implicit class NearInt(val i: Int) {
    def isNear(j: Int, within: Int = searchWindowSize / 2): Boolean = Range(j - within, j + within).contains(i)
  }

  implicit class WindowableSampleList(list: List[Int]) {
    def near(position: Int): List[Int] = list.slice(position - (searchWindowSize / 2), position + (searchWindowSize / 2) + 1)
  }

}