package pl.peksa.eiti.inz.backend.signals.endpoint

import java.time.ZonedDateTime

import org.json4s.native.Serialization.write
import org.scalatest.{Matchers, WordSpec}
import pl.peksa.eiti.inz.backend.signals.responses.{Frequency, SignalPart}

class JsonSupportSpec extends WordSpec with Matchers with JsonSupport {

  "json formats" when {
    "Samples" should {
      "serialize sample values as array" in {
        val startTime = ZonedDateTime.parse("2014-08-20T15:33:10+01:00")
        val samples = new SignalPart(startTime, new Frequency(360.0), Seq(1.0, 2.0, 3.0))
        val expectedJson = """{"startTime":"2014-08-20T15:33:10+01:00","samplingRate":360.0,"values":[1.0,2.0,3.0]}"""
        expectedJson should equal(write(samples))
      }
    }
  }

}
