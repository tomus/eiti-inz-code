package pl.peksa.eiti.inz.backend.signals.processing.filters

import org.scalatest.{Matchers, WordSpec}
import pl.peksa.eiti.inz.backend.signals.processing.filters.QrsDetection.{QrsDifferentator, HighPassFilter, LowPassFilter}

class FiltersTest extends WordSpec with Matchers {

  def impulseSignal(length: Int) = 1.0 +: List.fill(length-1)(0.0)

  "An LowPassFilter" should {
    "have valid impulse response" in {
      val input = impulseSignal(12)
      val output = input.map(new LowPassFilter)
      output should be(List(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0, 0.0))
    }
  }
  "An HighPassFilter" should {
    "have valid impulse response" in {
      val input = impulseSignal(32)
      val output = input.map(new HighPassFilter)
      output should be(List(-0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, 0.96875, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125, -0.03125))
    }
  }
  "An QrsDifferentator" should {
    "have valid impulse response" in {
      val input = impulseSignal(4)
      val output = input.map(new QrsDifferentator)
      output should be(List(0.25, 0.125, 0.0, -0.125))
    }
  }

}
