package pl.peksa.eiti.inz.backend.signals.testing

object SignalTestHelpers {
  def countConsecutiveSameValues(iter: Iterator[Double]): Iterator[Int] = {
    var previous = iter.next()
    var counter = 1
    (iter ++ List(Double.NaN).iterator).flatMap { current =>
      if ((current - previous).abs <= 1e-8) {
        counter += 1
        List.empty
      } else {
        val length = counter
        previous = current
        counter = 1
        List(length)
      }
    }
  }

}
