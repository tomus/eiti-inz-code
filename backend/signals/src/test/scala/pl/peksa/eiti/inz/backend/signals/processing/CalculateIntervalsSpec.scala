package pl.peksa.eiti.inz.backend.signals.processing

import org.scalatest.{Matchers, FunSuite}
import pl.peksa.eiti.inz.backend.signals.processing.filters.RrInterval
import RrInterval._

class CalculateIntervalsSpec extends FunSuite with Matchers {

  def intervals(values: Double*): List[Int] = {
    values.map(new IntervalCalculator).toList
  }

  test("should count low samples between high ones") {
    intervals() should equal(List())
    intervals(0) should equal(List(0))
    intervals(1) should equal(List(-1))
    intervals(0, 0, 0) should equal(List(0, 1, 2))
    intervals(1, 0, 1, 0) should equal(List(-1, 0, -1, 0))
    intervals(1, 1, 0, 1, 0, 0, 0, 1, 0) should equal(List(-1, -1, 0, -1, 0, 1, 2, -1, 0))
    intervals(1, 0, 1, 1, 0) should equal(List(-1, 0, -1, -1, 0))
    intervals(1, 0, 0, 1, 0) should equal(List(-1, 0 ,1, -1, 0))
    intervals(1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1) should equal(List(-1, -1, 0, 1, -1, 0, -1, -1, -1, 0, -1))
    intervals(0, 1, 0) should equal(List(0, -1, 0))
    intervals(1, 0, 1, 0) should equal(List(-1, 0, -1, 0))
    intervals(0, 1, 1, 0) should equal(List(0, -1, -1, 0))
    intervals(0, 0, 1, 0) should equal(List(0, 1, -1, 0))
    intervals(0, 1, 1, 0, 0, 1, 0, 1, 0) should equal(List(0, -1, -1, 0, 1, -1, 0, -1, 0))
  }
}
