package pl.peksa.eiti.inz.backend.signals.processing.actors

import java.time.ZonedDateTime
import java.util.UUID

import akka.testkit._
import pl.peksa.eiti.inz.backend.signals.processing.actors.SignalActor.NewSignalPartReceived
import pl.peksa.eiti.inz.backend.signals.responses.{Frequency, SignalPart, SignalWithSamples}
import pl.peksa.eiti.inz.backend.signals.testing.ActorSpec

class SignalActorSpec extends ActorSpec {
  val actorRef = system.actorOf(SignalActor.props)
  val subscriber = TestProbe()
  val dataRepository = TestProbe()
  //todo change name
  val testSignal = new SignalWithSamples(1, "", "", new SignalPart(ZonedDateTime.now(), new Frequency(1), Iterable.empty))
  val testRecordId: UUID = UUID.randomUUID()

  "signal actor" when {
    "has subscriber" should {
      actorRef.tell(SignalActor.SignalSubscription(testRecordId, 1), subscriber.ref)
      "inform subscribers about new samples" in {
        actorRef ! SignalActor.NewSignalPartReceived(testRecordId, testSignal)
        subscriber.expectMsgAllClassOf(classOf[NewSignalPartReceived])
      }

      "persist incoming data" ignore {
        actorRef ! "data"
        dataRepository.expectMsg("store data")
      }
    }
  }
}