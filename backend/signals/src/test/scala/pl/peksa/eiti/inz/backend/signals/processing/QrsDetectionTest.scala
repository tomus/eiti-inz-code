package pl.peksa.eiti.inz.backend.signals.processing

import java.io.StringWriter
import java.math.{RoundingMode, BigDecimal}

import com.github.tototoshi.csv.CSVReader
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{FunSuite, Matchers}
import pl.peksa.eiti.inz.backend.signals.processing.filters.QrsDetection._
import pl.peksa.eiti.inz.backend.signals.testing.PhysioBankAnnotations._
import pl.peksa.eiti.inz.backend.signals.testing.{ComparisonTest, LatexTable, Row}

class QrsDetectionTest extends FunSuite with Matchers with TableDrivenPropertyChecks with ComparisonTest {

  import pl.peksa.eiti.inz.backend.signals.testing.SignalCsvReader._

  //  val records = Table("recordName", "100", "101", /*"102", */"103", /*"104", */"105", "106")//, "107", "108", "109", "111", "112", "113", "114", "115", "116", "117", "118", "119", "121", "122", "123", "124", "200", "201", "202", "203", "205", "207", "208", "209", "210", "212", "213", /*"214", */"215", "217", "219", "220", "221", "222", "223", "228", "230", "231", "232", "233", "234")
  //  val records = Table("recordName", "221")
  val records = Table("recordName", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "111", "112", "113", "114", "115", "116", "117", "118", "119", "121", "122", "123", "124", "200", "201", "202", "203", "205", "207", "208", "209", "210", "212", "213", /*"214",*/ "215", "217", "219", "220", "221", "222", "223", /*"228", */ "230", /*"231", */ "232", "233", "234")
  val referenceSource = Table("referenceSource", "atr", "qrs")

  private[this] def calculateRSignal(recordName: String): Option[List[Int]] = {
    val inputSignals = readSignals(csvForRecord(recordName))
    val mliiSignal = inputSignals.find(s => s.name.contains("MLII"))
    mliiSignal.map {
      _.samples.values
        .map(new LowPassFilter)
        .map(new HighPassFilter)
        .map(new QrsDifferentator)
        .map(new QrsSquare)
        .map(new RDetector)
        .map(new RFilter)
        .drop(21)
        .toList
    }
  }


  private[this] def readRPositions(recordName: String, referenceSourceName: String): Iterable[Int] = {
    val reader: CSVReader = CSVReader.open(csvForRecord(s"$recordName-$referenceSourceName"))
    reader.toStream().map(_(0).toInt)
  }

  val searchWindowSize = (0.2 * 360).toInt
  val rPoint = 1.0

  def compare(recordName: String, rSignal: List[Int], reference: Iterable[Annotation], referenceName: String): TestResult = {
    val wasRFound = reference.map { beatAnnotation =>
      rSignal.near(beatAnnotation.sampleNo).contains(rPoint)
    }
    val warRIncorrectlyFound = rSignal.iterator.zipWithIndex.map { case (sampleValue, sampleNo) =>
      val isRPoint = sampleValue > 0
      def isInReference = reference.exists(_.sampleNo isNear(sampleNo, searchWindowSize * 2))
      isRPoint && !isInReference
    }
    val result = TestResult(recordName, wasRFound.count(_ == true), reference.size, warRIncorrectlyFound.count(_ == true), referenceName)
    println(result.toFormattedString)
    result
  }

  def compare(recordName: String, source: Iterable[Annotation], reference: Iterable[Annotation], referenceName: String): TestResult = {
    val wasRFound = reference.map { beatAnnotation =>
      source.exists(_.sampleNo.isNear(beatAnnotation.sampleNo))
    }
    val warRIncorrectlyFound = reference.map { beatAnnotation =>
      !source.exists(_.sampleNo.isNear(beatAnnotation.sampleNo))
    }
    val result = TestResult(recordName, wasRFound.count(_ == true), reference.size, warRIncorrectlyFound.count(_ == true), referenceName)
    println(result.toFormattedString)
    result
  }

  test("detected R points are the same (within given distance)") {
    val allResults = records.toList.par.flatMap { recordName =>
      val maybeRSignal = calculateRSignal(recordName)
      val res = for {
        referenceSource <- referenceSource.toList
        reference = readAnnotations(recordName, referenceSource).filter(_.meaning.isBeat)
        if maybeRSignal.isDefined
      } yield compare(recordName, maybeRSignal.get, reference, referenceSource)
      val sqrsVsMan: TestResult = {
        val sqrs = readAnnotations(recordName, "qrs").filter(_.meaning.isBeat)
        val man = readAnnotations(recordName, "atr").filter(_.meaning.isBeat)
        compare(recordName, sqrs, man, "xxx")
      }
      if(res.isEmpty) res else res :+ sqrsVsMan
    }

    //val resultsByRecord = allResults.iterator.toList.groupBy(_.testName).map(_._2).toList.sortBy(_.head.recordName)
    val tables =
      allResults.iterator.toList.groupBy(_.testName)
        .map { case (testName: String, results: List[TestResult]) =>
        val caption = testName match {
          case "atr" => "Porównanie wykrycia punktów R z użyciem własnej implementacji i oznaczeń kardiologów"
          case "qrs" => "Porównanie wykrycia punktów R z użyciem własnej implementacji i programu SQRS"
          case "xxx" => "Ocena wyników progranu SQRS względem ręcznego oznaczenia"
        }
        val label = testName match {
          case "atr" => "tab:result:man"
          case "qrs" => "tab:result:sqrs"
          case "xxx" => "tab:result:manvsqrs"
        }
        val header = new Row(List(
          "Nazwa nagrania",
          "Liczba wykrytych",
          "Liczba spodziewanych",
          "\\%",
          "Liczba fałszywie wykrytych"
        ))

        val rows = results.map { result =>
          new Row(List(
            result.recordName,
            result.foundCount,
            result.referenceCount,
            result.efficiency.multiply(BigDecimal.valueOf(100)).setScale(2),
            result.falsePositiveCount
          ))
        }
        new LatexTable(caption, "|l||c|c|c|c|", header, label, rows)
      }.toList.sortBy {
        _.label match {
          case "tab:result:man" => 2
          case "tab:result:sqrs" => 1
          case "tab:result:manvsqrs" => 3
        }
      }

    val avgPercents = allResults.iterator.toList.groupBy(_.testName)
      .map { case (testName: String, results: List[TestResult]) =>
      val allFound = results.map(_.foundCount).sum
      val allReference = results.map(_.referenceCount).sum
      val allFalsePositives = results.map(_.falsePositiveCount).sum
      val allPercent = BigDecimal.valueOf(allFound).divide(BigDecimal.valueOf(allReference), 4, RoundingMode.HALF_UP)
      testName -> (allFound, allReference, allPercent, allFalsePositives)
    }
    println(avgPercents)
    val writer = new StringWriter()
    tables.foreach(_.write(writer))
    println(writer.toString)
  }

}
