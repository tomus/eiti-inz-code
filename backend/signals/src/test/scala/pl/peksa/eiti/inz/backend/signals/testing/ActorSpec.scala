package pl.peksa.eiti.inz.backend.signals.testing

import akka.actor.ActorSystem
import akka.testkit._
import org.scalatest.{Matchers, WordSpecLike}

abstract class ActorSpec extends TestKit(ActorSystem("test-actorSystem")) with DefaultTimeout with ImplicitSender with WordSpecLike with Matchers