

import java.io._
import java.nio.file.Files
import java.time.ZonedDateTime

import com.github.tototoshi.csv.CSVReader
import com.google.common.base.Charsets
import pl.peksa.eiti.inz.backend.signals.processing.filters.{MovingWindowIntegrator, RrInterval, QrsDetection}
import pl.peksa.eiti.inz.backend.signals.processing.filters.RrInterval.{IntervalCalculator, ArrhythmiaDetector}
import pl.peksa.eiti.inz.backend.signals.processing.filters.QrsDetection._
import pl.peksa.eiti.inz.backend.signals.responses.{Frequency, SignalPart, SignalWithSamples}
import pl.peksa.eiti.inz.backend.signals.testing.SignalCsvReader

object ProcessingTest extends App with SignalCsvReader with SignalGnuplotPlotter {

  val signalName = "106"

  val inputSignals = readSignals(csvForRecord(signalName))
  val timeSignal = inputSignals.filter(_.no == 0).head
  val mliiSignal = inputSignals.filter(_.name.contains("MLII")).head


  val filtered = mliiSignal.samples.values
    .map(new LowPassFilter)
    .map(new HighPassFilter)
    .toList
  val differentiated = filtered
    .map(new QrsDifferentator)
    .toList
  val squared = differentiated
    .map(new QrsSquare)
    .toList
  val r = squared
    .map(new RDetector)
    .map(new RFilter)
    .map(_.toDouble)
    .toList
  val intervals = r
    .map(new IntervalCalculator)
    .toList
  val classes = intervals
    .map(new ArrhythmiaDetector)
    .map(x => 50+(x*10))
    .toList

  val outputSignals = Set(
    mliiSignal,
    new SignalWithSamples(2, "n/a", "filtered", new SignalPart(null, new Frequency(1), filtered)),
    new SignalWithSamples(3, "n/a", "diff", new SignalPart(null, new Frequency(1), differentiated)),
    new SignalWithSamples(4, "n/a", "squared", new SignalPart(null, new Frequency(1), squared)),
    //new SignalWithSamples(5, "n/a", "integrated", new SignalPart(null, new Frequency(1), integrator(x).toStream)),
    new SignalWithSamples(5, "n/a", "r", new SignalPart(null, new Frequency(1), r.map(_*70))),
    new SignalWithSamples(7, "n/a", "rrclass", new SignalPart(null, new Frequency(1), classes.map(_.toDouble))),
    new SignalWithSamples(8, "n/a", "intervals", new SignalPart(null, new Frequency(1), intervals.map(_.toDouble)))
  )
  plot(outputSignals, signalName)
}

trait SignalGnuplotPlotter {
  private[this] def gnuplotScript(dataFile: String, signals: Set[SignalWithSamples], recordName: String) = {
    var counter = 0
    val plotDefinitions = signals.toList.sortBy(_.no).map { signal =>
      counter += 1
      s""""$dataFile" using 0:$counter title '${signal.name.replace("'", "")}' with lines"""
    }
    val plotDefinitionString = plotDefinitions mkString ",\\\n"
    s"""
set terminal png size ${Math.min(signals.head.samples.valueSource.size, 400000)},980 enhanced font "Helvetica,20"
set output 'output.png'
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set xlabel "T (s)"
set ylabel "U (mV)"
set key left top
plot "${recordName}ann.tsv" using 1:2:xtic(3) title 'annotations' with points,\\
$plotDefinitionString;
     """
  }

  def toTsv(signals: Set[SignalWithSamples]): Iterator[String] = {
    val sortedSignals = signals.toList.sortBy(_.no)
    val columns = sortedSignals.map { signal =>
      Seq(signal.name, s"${signal.unit}@${signal.samples.samplingRate.value}").iterator ++ signal.samples.values.map(_.toString)
    }
    new Iterator[String] {
      override def hasNext: Boolean = {
        columns.map(_.hasNext).fold(false)(_ || _)
      }

      override def next(): String = {
        columns.map(_.next()) mkString "\t"
      }
    }
  }

  def plot(signals: Set[SignalWithSamples], recordName: String) = {
    withTempFile("plt", { scriptFile: File =>
      withTempFile("tsv", { dataFile: File =>
        writeFile(scriptFile, gnuplotScript(dataFile.getAbsolutePath, signals, recordName))
        writeFile(dataFile, toTsv(signals))
        println(s"${scriptFile.getAbsolutePath} ${dataFile.getAbsolutePath}")
      })
    })
  }

  private[this] def writeFile(file: File, text: String): Unit = {
    Files.write(file.toPath, text.getBytes(Charsets.UTF_8))
  }

  private[this] def writeFile(file: File, lines: Iterator[String]): Unit = {
    val writer: FileWriter = new FileWriter(file)
    lines.foreach(writer.append(_).append("\n"))
    writer.close()
  }

  private[this] def withTempFile(extension: String, f: (File => Unit)): Unit = {
    val file = File.createTempFile("ecgplot", s".$extension")
    try {
      f(file)
    } finally {
      //file.delete()
    }
  }

}