package pl.peksa.eiti.inz.backend.signals

import java.time.ZonedDateTime
import java.util.UUID

import akka.actor.Props
import com.typesafe.config.ConfigFactory
import org.scalatest.{Matchers, WordSpec}
import pl.peksa.eiti.inz.backend.signals.cassandra.{ConfigCassandraCluster, RecordRepositoryActor}
import pl.peksa.eiti.inz.backend.signals.commands.{CNR_Signal, CreateNewRecord}
import pl.peksa.eiti.inz.backend.signals.endpoint.{JsonSupport, SignalsEndpoint}
import pl.peksa.eiti.inz.backend.signals.processing.actors.{RecordActor, IdBasedDispatchingActor, IdBasedDispatchingActor$}
import pl.peksa.eiti.inz.backend.signals.responses.NewRecordCreated
import spray.http.{HttpData, ContentTypes, HttpEntity}
import spray.http.StatusCodes._
import spray.testkit.ScalatestRouteTest

import scala.concurrent.duration._
import scala.util.Random

class EndpointSpecification extends WordSpec with ScalatestRouteTest with Matchers with SignalsEndpoint with JsonSupport with ConfigCassandraCluster {
  def actorRefFactory = system

  implicit val routeTestTimeout = RouteTestTimeout(15.seconds)

  system.actorOf(RecordRepositoryActor.props(cluster), "recordRepository")
  system.actorOf(IdBasedDispatchingActor.props[UUID]("record", _ => RecordActor.props), "recordManager")

  val testCreateNewRecord = CreateNewRecord(Set(CNR_Signal(1, 360, "mV", "MLII"), CNR_Signal(2, 1, "beats/minute", "heart rate")))
  val testSamplesProvider = new TestSamplesProvider(testCreateNewRecord.signals)
  var createdRecordId: UUID = null
  val recordSamplesStartTime = ZonedDateTime.now()
  val sampleCount = (testCreateNewRecord.signals.head.samplingRate * (1 minute).toSeconds).toInt

  "Signals endpoint" should {
    "save new record" in {
      Post("/record", testCreateNewRecord) ~> endpoint ~> check {
        status should be(Created)
        createdRecordId = responseAs[NewRecordCreated].recordId
      }
    }
    "return record metadata" in {
      assert(createdRecordId != null)
      Get(s"/record/$createdRecordId") ~> endpoint ~> check {
        status should be(OK)
        responseAs[CreateNewRecord] should equal(testCreateNewRecord)
      }
    }
    "store multichannel signal samples" in {
      val samples = testSamplesProvider.take(sampleCount + 1).toSeq
      val request = (recordSamplesStartTime.toString +: samples) mkString "\n"
      Post(s"/record/$createdRecordId/samples").copy(entity = HttpEntity(request)) ~> endpoint ~> check {
        status should be(OK)
        body.asString should include(s"Added $sampleCount samples")
      }
    }
    "provide signal samples for given time frame and selected signal" in {
      val offset = "-10s" //todo not implemented
      val duration = "-10s" //todo not implemented
      Get(s"/record/$createdRecordId/samples?offset=$offset&duration=$duration") ~> endpoint ~> check {
        status should be(OK)
        body.asString should startWith(createdRecordId.toString)
        val bodyLines = body.asString.split("\\r?\\n")
        for (i <- 1 until bodyLines.size) {
          bodyLines(i).split(',') should have size 2 // 2 signals
        }
        bodyLines should have length sampleCount + 2 // + 2 - recordId and csv header
      }
    }
  }

  class TestSamplesProvider(val signals: Iterable[CNR_Signal]) extends Iterable[String] {
    override def iterator: Iterator[String] = new Iterator[String] {
      override def hasNext: Boolean = true

      private[this] var isFirst = true

      val header = signals.map { signal => s"${signal.no}#${signal.name}[${signal.unit}]@${signal.samplingRate}"} mkString ","

      private[this] val random = new Random()

      private[this] def randomDouble() = random.nextDouble()

      def randomSamples(): String = {
        signals.map(_ => randomDouble()) mkString ","
      }

      override def next(): String = {
        val line = if (isFirst) header else randomSamples()
        isFirst = false
        line
      }
    }
  }


}
