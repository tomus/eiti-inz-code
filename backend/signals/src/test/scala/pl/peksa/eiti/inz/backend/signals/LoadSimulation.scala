package pl.peksa.eiti.inz.backend.signals

import java.time.ZonedDateTime

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

abstract class LoadSimulation(val chunkSize: Int, val userCount: Int) extends Simulation {

  val paceDuration = (1000 * chunkSize.toDouble / 360.0).milliseconds

  def readHosts: List[String] = scala.io.Source.fromFile("/home/tomek/private/eiti-inz/dockers/signal_nodes.txt").getLines().toList

  val host = Iterator.continually(readHosts).flatten

  implicit class SessionParams(val session: Session) {

    def samples: Iterator[String] = session("samples").as[Iterator[String]]

    def samples(samples: Iterator[String]): Session = session.set("samples", samples)

    def host: String = session("host").as[String]

    def host(host: String): Session = {
      session.set("host", host)
    }

    def recordId: String = session("recordId").as[String]
  }

  val createRecordJson: String = """{"signals":[{"no":0,"samplingRate":360,"unit":"s","name":"elapsed time"},{"no":1,"samplingRate":360,"unit":"mV","name":"MLII"},{"no":2,"samplingRate":360,"unit":"mV","name":"V5"}]}"""

  val createRecord = exec(http("create record")
    .post("http://${host}:8080/record").body(StringBody(createRecordJson)).asJSON
    .check(status.is(201))
    .check(jsonPath("$.recordId").ofType[String].saveAs("recordId"))
  )

  val lines = scala.io.Source.fromFile("/home/tomek/private/eiti-inz/eiti-inz-code/mit-bih/100.csv").getLines().drop(2).take(360 * 60 * 5).toList

  def readSamples: Iterator[String] = lines.iterator

  val uploadSamples = exec { session: Session =>
    session.samples(readSamples)
  }.asLongAs(_("samples").as[Iterator[_]].hasNext) {
    pace(paceDuration).exec(http(_ => s"upload $chunkSize samples")
      .post("http://${host}:8080/record/${recordId}/samples")
      .body(StringBody({ session =>
      val sb = new StringBuilder(chunkSize * 2 + 1)
      sb ++= s"${ZonedDateTime.now()}\n0#elapsedTime[sec],1#MLII[mV]@360.0,2#V5[mV]@360.0\n"
      session.samples.take(chunkSize).foreach(sb ++= _ ++= "\n")
      sb.toString()
    }))
      .check(status.is(200))
    )
  }

  val httpConf = http
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // 6
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")

  val createAndUpload = exitBlockOnFail(exec(createRecord, uploadSamples))

  def testScenario(size: Int) = scenario(s"create and upload samples, $size per request")
    .exec(
      exec(_.host(host.next())),
      createAndUpload
    )

  setUp(
    testScenario(chunkSize)
      .inject(rampUsers(userCount) over 1.minute)
  ).protocols(httpConf)
//    .maxDuration(FiniteDuration((readSamples.size / 360) / 2, SECONDS))

}

class Load10SamplesBy100UsersSimulation extends LoadSimulation(10, 100)

class Load100SamplesBy100UsersSimulation extends LoadSimulation(100,100)

class Load5SecBy100UsersSimulation extends LoadSimulation(360 * 5, 100)

class Load10SecBy100UsersSimulation extends LoadSimulation(360 * 10, 100)

class Load30SecBy100UsersSimulation extends LoadSimulation(360 * 30, 100)

class Load1MinBy100UsersSimulation extends LoadSimulation(360 * 60, 100)

class Load2MinBy100UsersSimulation extends LoadSimulation(360 * 60 * 2, 100)

class Load10SamplesBy500UsersSimulation extends LoadSimulation(10, 500)

class Load100SamplesBy500UsersSimulation extends LoadSimulation(100, 500)

class Load5SecBy500UsersSimulation extends LoadSimulation(360 * 5, 500)

class Load10SecBy500UsersSimulation extends LoadSimulation(360 * 10, 500)

class Load30SecBy500UsersSimulation extends LoadSimulation(360 * 30, 500)

class Load1MinBy500UsersSimulation extends LoadSimulation(360 * 60, 500)

class Load2MinBy500UsersSimulation extends LoadSimulation(360 * 60 * 2, 500)

class Load10SamplesBy200UsersSimulation extends LoadSimulation(10, 200)

class Load100SamplesBy200UsersSimulation extends LoadSimulation(100, 200)

class Load5SecBy200UsersSimulation extends LoadSimulation(360 * 5, 200)

class Load10SecBy200UsersSimulation extends LoadSimulation(360 * 10, 200)

class Load30SecBy200UsersSimulation extends LoadSimulation(360 * 30, 200)

class Load1MinBy200UsersSimulation extends LoadSimulation(360 * 60, 200)

class Load2MinBy200UsersSimulation extends LoadSimulation(360 * 60 * 2, 200)

class Load10SamplesBy1000UsersSimulation extends LoadSimulation(10, 1000)

class Load100SamplesBy1000UsersSimulation extends LoadSimulation(100, 1000)

class Load5SecBy1000UsersSimulation extends LoadSimulation(360 * 5, 1000)

class Load10SecBy1000UsersSimulation extends LoadSimulation(360 * 10, 1000)

class Load30SecBy1000UsersSimulation extends LoadSimulation(360 * 30, 1000)

class Load1MinBy1000UsersSimulation extends LoadSimulation(360 * 60, 1000)

class Load2MinBy1000UsersSimulation extends LoadSimulation(360 * 60 * 2, 1000)


class Load1MinBy300UsersSimulation extends LoadSimulation(360 * 60, 300)
class Load1MinBy400UsersSimulation extends LoadSimulation(360 * 60, 400)
class Load1MinBy600UsersSimulation extends LoadSimulation(360 * 60, 600)
class Load1MinBy700UsersSimulation extends LoadSimulation(360 * 60, 700)
class Load1MinBy800UsersSimulation extends LoadSimulation(360 * 60, 800)
class Load1MinBy900UsersSimulation extends LoadSimulation(360 * 60, 900)

