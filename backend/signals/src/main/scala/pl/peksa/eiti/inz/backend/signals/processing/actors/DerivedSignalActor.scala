package pl.peksa.eiti.inz.backend.signals.processing.actors

import akka.actor.{Actor, ActorRef, Props}
import com.typesafe.scalalogging.StrictLogging
import pl.peksa.eiti.inz.backend.signals.processing.actors.SignalActor.{NewSignalPartReceived, SignalSubscription}
import pl.peksa.eiti.inz.backend.signals.responses.SignalPart

class DerivedSignalActor(val sourceSignalActor: ActorRef, val processor: Iterator[Double] => Iterator[Double], val signalNo: Int) extends Actor with StrictLogging {

  override def preStart(): Unit = {
    sourceSignalActor ! SignalActor.SignalSubscription(java.util.UUID.randomUUID(), signalNo)
  }

  private[this] var subscribers = List.empty[ActorRef]

  override def receive: Receive = {
    case SignalSubscription(recordId, signalId) =>
      logger.info(s"${sender()} subscribed to $recordId, $signalId")
      subscribers = sender() +: subscribers
    case msg: NewSignalPartReceived =>
      val samples = msg.signal.samples
      subscribers.foreach { subscriber =>
        val processedSamples = new SignalPart(samples.startTime, samples.samplingRate, processor(samples.values).toStream)
        val signalToSent = msg.signal.copy(samples = processedSamples, no = signalNo)
        val msgToSent = NewSignalPartReceived(msg.recordId, signalToSent)
        subscriber ! msgToSent
      }
    case x =>
      logger.error(s"Not handled $x from ${sender()} to $self")
  }
}

object DerivedSignalActor {
  def props(sourceSignalActor: ActorRef, processor: Iterator[Double] => Iterator[Double], signalNo: Int): Props = {
    Props(classOf[DerivedSignalActor], sourceSignalActor, processor, signalNo)
  }

}
