package pl.peksa.eiti.inz.backend.signals.cassandra

import java.net.{Inet4Address, InetAddress}

import akka.actor.ActorSystem
import com.datastax.driver.core.Cluster
import com.typesafe.config.ConfigFactory

import scala.collection.JavaConversions._

trait CassandraCluster {
  def cluster: Cluster
}

trait ConfigCassandraCluster extends CassandraCluster {
  def system: ActorSystem

  val config = ConfigFactory.load()

  private val port = config.getInt("arrhytmio.cassandra.port")
  private val hosts = config.getStringList("arrhytmio.cassandra.hosts").map { hostname =>
    InetAddress.getByName(hostname);
  }

  lazy val cluster: Cluster =
    Cluster.builder()
      .addContactPoints(hosts: _*)
      //      .withCompression(ProtocolOptions.Compression.LZ4)
      .withPort(port)
      .build()

}
