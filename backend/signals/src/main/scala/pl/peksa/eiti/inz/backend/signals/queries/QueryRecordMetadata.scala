package pl.peksa.eiti.inz.backend.signals.queries

import java.util.UUID

sealed case class QueryRecordMetadata(id: UUID)
