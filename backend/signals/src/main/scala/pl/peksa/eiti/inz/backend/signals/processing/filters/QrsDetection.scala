package pl.peksa.eiti.inz.backend.signals.processing.filters

import pl.peksa.eiti.inz.backend.signals.processing.filters.utils.FixedSizeMemory
import pl.peksa.eiti.inz.backend.signals.processing.filters.utils.FixedSizeMemory.InitialValue

object QrsDetection {

  class LowPassFilter extends RecursiveFilter[Double](12, 2) {
    //y(nT) = 2y(nT-T)-y(nT-2T)+x(nT)-2x(nT-6T)+x(nT-12T)
    override val output = (x: SampleMemory, y: SampleMemory) => 2 * y(-1) - y(-2) + x(0) - (2 * x(-6)) + x(-12)

    val delay = 5
  }


  class HighPassFilter extends RecursiveFilter[Double](32, 1) {
    //y(nT)=y(nT-T)-x(nT)/32 + x(nT - 16T) - x(nT - 17 T) + x(nT - 32T)/32
    override val output = (x: SampleMemory, y: SampleMemory) => y(-1) - (x(0) / 32) + x(-16) - x(-17) + (x(-32) / 32)

    val delay = 16
  }

  class QrsDifferentator extends Filter[Double](4) {
    //y(nT) = (2x(nT) + x(nT-T) - x(nT - 3T) - 2x(nT-4T)) / 8
    override val output = (x: SampleMemory) => (2 * x(0) + x(-1) - x(-3) - 2 * x(-4)) / 8
  }

  class QrsSquare extends Filter[Double](0) {
    override val output = (x: SampleMemory) => x(0) * x(0)
  }

  class RDetector extends Processor[Double, Int] {
    var spki = 8.0
    var npki = 0.0
    //val buffer = FixedSizeMemory.createInitialized[Int](10, InitialValue(20))

    def treshold = 8 //npki + ((spki - npki) / 4)

    override def apply(v: Double): Int = {
      if (v > treshold) {
        spki = (0.125 * v) + (0.875 * spki)
        1
      } else {
        npki = (0.125 * v) + (0.875 * npki)
        0
      }
    }
  }

  class RFilter extends Filter[Int]((360 * 0.2).toInt) {
    override val output = (x: SampleMemory) => {
      val notFoundRecently: Boolean = xCurrentAndPreviousBuffer.getAll.tail.forall { a: Any => a match {
        //TODO to jest niewydajne
        case x: Double => x < 1.0
        case x: Int => x < 1
      }
      }
      if (x(0) > 0 && notFoundRecently)
        x(0)
      else
        0
    } //todo oversimplified?
  }

}
