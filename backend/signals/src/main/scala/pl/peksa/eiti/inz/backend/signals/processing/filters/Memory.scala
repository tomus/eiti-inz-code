package pl.peksa.eiti.inz.backend.signals.processing.filters

import pl.peksa.eiti.inz.backend.signals.processing.filters.Processor._
import pl.peksa.eiti.inz.backend.signals.processing.filters.utils.FixedSizeMemory
import pl.peksa.eiti.inz.backend.signals.processing.filters.utils.FixedSizeMemory.InitialValue

trait Memory[T] {

  type SampleMemory = SampleOffset => T

  val initialValue = new InitialValue[T](0.0.asInstanceOf[T])

}

trait InputMemory[T] extends Memory[T] {

  val previousInputMemorySize: Int
  private[this] val xBufferSize = previousInputMemorySize + 1
  private[filters] val xCurrentAndPreviousBuffer = FixedSizeMemory.createInitialized[T](xBufferSize, initialValue)

  def inputMemory(index: SampleOffset): T = {
    assert(index <= 0, "input memory can reference only previous samples or current one (with non-positive index")
    xCurrentAndPreviousBuffer.get(-index)
  }

  def memorizeInput(value: T) = xCurrentAndPreviousBuffer.add(value)

}

trait OutputMemory[T] extends Memory[T] {

  val previousOutputMemorySize: Int
  private[this] val yBufferSize = previousOutputMemorySize
  private[this] val yPreviousBuffer = FixedSizeMemory.createInitialized[T](yBufferSize, initialValue)

  def outputMemory(index: SampleOffset): T = {
    assert(index <= 0, "output memory can reference only previous samples (with negative index")
    yPreviousBuffer.get(-(index+1))
  }

  def memorizeOutput(value: T) = yPreviousBuffer.add(value)

}

