package pl.peksa.eiti.inz.backend.signals.responses

import java.time.ZonedDateTime
import java.util.UUID

sealed case class RecordSamples(recordId: UUID, signals: Set[SignalWithSamples])
sealed case class SignalWithSamples(no: Int, unit: String, name: String, samples: SignalPart)

sealed class SignalPart(val startTime: ZonedDateTime, val samplingRate: Frequency, val valueSource: Iterable[Double]) {
  def values: Iterator[Double] = valueSource.iterator
}
class Frequency(val value: Double) extends AnyVal
