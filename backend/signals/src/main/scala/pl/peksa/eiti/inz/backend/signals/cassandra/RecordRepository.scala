package pl.peksa.eiti.inz.backend.signals.cassandra

import java.time.ZonedDateTime
import java.util.concurrent.TimeUnit
import java.util.{Date, UUID}

import com.datastax.driver.core.{PreparedStatement, ResultSet, Row}
import com.typesafe.scalalogging.StrictLogging
import pl.peksa.eiti.inz.backend.signals.cassandra.FutureUtils._
import pl.peksa.eiti.inz.backend.signals.commands.{CNR_Signal, CreateNewRecord, SampleData}
import pl.peksa.eiti.inz.backend.signals.responses._

import scala.collection.JavaConversions._
import scala.concurrent.Future
import scala.concurrent.duration.{Duration, FiniteDuration}

trait RecordRepositoryComponent {
  def repository: RecordRepository

  trait RecordRepository {

    def store(command: CreateNewRecord): Future[NewRecordCreated]

    def readMetadata(recordId: UUID): Future[CreateNewRecord]

    def storeSamples(recordId: UUID, data: SampleData): Future[_]

    def querySamples(recordId: UUID, duration: Duration, offset: FiniteDuration): Future[RecordSamples]

  }

}

trait CassandraRecordRepositoryComponent extends RecordRepositoryComponent {
  this: CassandraClusterComponent =>

  override def repository: RecordRepository = CassandraRecordRepository

  import scala.concurrent.ExecutionContext.Implicits.global

  object CassandraRecordRepository extends RecordRepository with StrictLogging {
    val session = cassandraCluster.connect(CassandraKeySpaces.recordKeySpace)

    private[this] val insertSamples = session.prepare(
      """INSERT INTO record_samples(record_id, signal_no, date, samples) VALUES (?, ?, ?, ?)"""
    ).enableTracing()

    private[this] val insertRecord = session.prepare(
      """INSERT INTO record_metadata(record_id, signal_no, sampling_rate, unit, signal_name) VALUES (?, ?, ?, ?, ?)"""
    ).enableTracing()

    private[this] val selectRecord = session.prepare(
      """SELECT record_id, signal_no, sampling_rate, unit, signal_name FROM record_metadata WHERE record_id = ?"""
    ).enableTracing()

    private[this] val selectSamples = session.prepare(
      """SELECT samples FROM record_samples WHERE record_id = ? and signal_no = ?"""
    ).enableTracing()

    private[this] def rowToCNR_Signal(row: Row): CNR_Signal = {
      CNR_Signal(row.getInt("signal_no"), row.getDouble("sampling_rate"), row.getString("unit"), row.getString("signal_name"))
    }

    private[this] def executeAsyncQuery(statement: PreparedStatement, args: AnyRef*): Future[ResultSet] = {
      val boundStatement = statement.bind(args: _ *)
      val result = session.executeAsync(boundStatement).toPromise
      logFutureResultStatistics(result, statement, args)
      result
    }

    private[this] def logFutureResultStatistics(future: Future[ResultSet], statement: PreparedStatement, args: Any*): Unit = {
      val stopwatch = com.google.common.base.Stopwatch.createStarted()
      future.onComplete { result =>
        stopwatch.stop()
        logger.debug(s"Query ${statement.getQueryString} took ${stopwatch.elapsed(TimeUnit.MILLISECONDS)}ms.")
        logger.trace(s"Query args were: $args")
        logger.trace(s"Query result: $result")
      }
    }

    private[this] def logFutureStatistics(future: Future[_], operationName: String, args: Any*): Unit = {
      val stopwatch = com.google.common.base.Stopwatch.createStarted()
      future.onComplete { result =>
        stopwatch.stop()
        logger.debug(s"$operationName took ${stopwatch.elapsed(TimeUnit.MILLISECONDS)}ms.")
        logger.trace(s"$operationName args were: $args")
        logger.trace(s"$operationName result: $result")
      }
    }

    private[this] def storeSignal(recordId: UUID, signal: CNR_Signal): Future[ResultSet] = {
      executeAsyncQuery(insertRecord,
        recordId,
        signal.no.asInstanceOf[Integer],
        signal.samplingRate.asInstanceOf[java.lang.Double],
        signal.unit,
        signal.name
      )
    }

    override def store(command: CreateNewRecord): Future[NewRecordCreated] = {
      val newRecordId = UUID.randomUUID()
      val results = for {
        signal <- command.signals
        result = storeSignal(newRecordId, signal)
      } yield result
      Future.sequence(results).map { _ => NewRecordCreated(newRecordId)}
    }

    override def readMetadata(recordId: UUID): Future[CreateNewRecord] = {
      executeAsyncQuery(selectRecord, recordId)
        .map { resultSet =>
        CreateNewRecord(resultSet.all().map(rowToCNR_Signal).toSet)
      }
    }

    override def storeSamples(recordId: UUID, data: SampleData): Future[_] = {
      val results = for {
        i <- 0 until data.signals.size
        signal = data.signals(i)
        signalNo = signal.substring(0, signal.indexOf('#')).toInt.asInstanceOf[java.lang.Integer]
        samples = data.samples.map(_(i)) mkString ","
        date = Date.from(data.dateTime.toInstant)
        result = executeAsyncQuery(insertSamples, recordId, signalNo, date, samples)
      } yield result
      Future.sequence(results).map(_ => None)
    }

    implicit class FutureSet[A](val futureSet: Future[Set[A]]) {
      def flatMapEach[B](f: A => Future[B]): Future[Set[B]] = {
        futureSet.flatMap { set: Set[A] =>
          Future.sequence(set.map(f))
        }
      }
    }

    override def querySamples(recordId: UUID, duration: Duration, offset: FiniteDuration): Future[RecordSamples] = {
      // TODO implement offset
      val signalsFuture = readMetadata(recordId).map(_.signals)
      val signalsWithSamplesFuture = signalsFuture.flatMapEach { signal =>
        val samples = readSamples(recordId, signal.no, signal.samplingRate)
        samples.map(SignalWithSamples(signal.no, signal.unit, signal.name, _))
      }
      val result = signalsWithSamplesFuture.map(new RecordSamples(recordId, _))
      logFutureStatistics(result, "querySamples", recordId)
      result
    }

    private[this] def readSamples(recordId: UUID, signalNo: Int, samplingRate: Double): Future[SignalPart] = {
      val queryResult = executeAsyncQuery(selectSamples, recordId, signalNo.asInstanceOf[java.lang.Integer])
      queryResult.map { resultSet =>
        val sampleSource = resultSet.flatMap(_.getString("samples").split(',').map(_.toDouble))
        new SignalPart(ZonedDateTime.now(), new Frequency(samplingRate), sampleSource)
      }
    }
  }

}