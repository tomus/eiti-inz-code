package pl.peksa.eiti.inz.backend.signals

trait Identifiable[+T] {
  val id: T
}
