package pl.peksa.eiti.inz.backend.signals.commands

import java.time.ZonedDateTime
import java.util.UUID

sealed case class AddSamples(recordId: UUID, sampleData: SampleData)
sealed case class SampleData(dateTime: ZonedDateTime, signals: Array[String], samples: Seq[Array[Double]])
