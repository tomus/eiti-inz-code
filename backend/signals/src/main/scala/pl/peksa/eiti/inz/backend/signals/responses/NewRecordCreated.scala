package pl.peksa.eiti.inz.backend.signals.responses

import java.util.UUID

sealed case class NewRecordCreated(recordId: UUID)
