package pl.peksa.eiti.inz.backend.signals.processing.actors

import java.util.UUID

import akka.actor.{Actor, ActorRef, Props}
import com.typesafe.scalalogging.StrictLogging
import pl.peksa.eiti.inz.backend.signals.Identifiable
import pl.peksa.eiti.inz.backend.signals.processing.filters.RrInterval.{ArrhythmiaDetector, IntervalCalculator}
import pl.peksa.eiti.inz.backend.signals.processing.filters.{RrInterval, MovingWindowIntegrator}
import pl.peksa.eiti.inz.backend.signals.processing.filters.QrsDetection._

class RecordActor extends Actor with StrictLogging {


  val filter: Iterator[Double] => Iterator[Double] = x => x.map(new LowPassFilter).map(new HighPassFilter)
  val differentiator: Iterator[Double] => Iterator[Double] = x => filter(x).map(new QrsDifferentator)
  val squarer: Iterator[Double] => Iterator[Double] = x => differentiator(x).map(new QrsSquare)
  val integrator: Iterator[Double] => Iterator[Double] = x => new MovingWindowIntegrator(squarer(x), 50)
  val rPoints: Iterator[Double] => Iterator[Double] = x => squarer(x).map(new RDetector).map(_.toDouble)
  val rrIntervals: Iterator[Double] => Iterator[Int] = x => rPoints(x).map(new IntervalCalculator)
  val detector: Iterator[Double] => Iterator[Double] = x => rrIntervals(x).map(new ArrhythmiaDetector).map(_.toDouble)//map(x => if(x > 0) 1.0 else 0.0)

  private[this] def childActorForSignal(signalNo: Int): ActorRef = {
    def createChild: ActorRef = {
      logger.trace(s"Creating SignalActor for ${self.toString()}, signal $signalNo")
      val signalActor = context.actorOf(SignalActor.props, s"signal-$signalNo")
      context.actorOf(DerivedSignalActor.props(signalActor, filter, 100-signalNo), s"signal-${100 - signalNo}")
      context.actorOf(DerivedSignalActor.props(signalActor, differentiator, 200-signalNo), s"signal-${200 - signalNo}")
      context.actorOf(DerivedSignalActor.props(signalActor, squarer, 300-signalNo), s"signal-${300 - signalNo}")
      context.actorOf(DerivedSignalActor.props(signalActor, integrator, 400-signalNo), s"signal-${400 - signalNo}")
      context.actorOf(DerivedSignalActor.props(signalActor, rPoints, 500-signalNo), s"signal-${500 - signalNo}")
      context.actorOf(DerivedSignalActor.props(signalActor, detector, 600-signalNo), s"signal-${600 - signalNo}")
      signalActor
    }
    context.child(s"signal-$signalNo").getOrElse(createChild)
  }

  override def receive: Receive = {
    case msg: SignalActor.SignalIdAwareMessage =>
      childActorForSignal(msg.signalNo).forward(msg)
      childActorForSignal(100 - msg.signalNo).forward(msg)
      childActorForSignal(200 - msg.signalNo).forward(msg)
      childActorForSignal(300 - msg.signalNo).forward(msg)
      childActorForSignal(400 - msg.signalNo).forward(msg)
      childActorForSignal(500 - msg.signalNo).forward(msg)
      childActorForSignal(600 - msg.signalNo).forward(msg)
    case x =>
      logger.error(s"Not handled $x")
  }
}

object RecordActor {
  def props: Props = Props(classOf[RecordActor])

  abstract class RecordIdAwareMessage(val recordId: UUID) extends Identifiable[UUID] {
    override val id: UUID = recordId
  }

}
