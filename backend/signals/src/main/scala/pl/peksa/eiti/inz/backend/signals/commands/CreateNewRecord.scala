package pl.peksa.eiti.inz.backend.signals.commands

sealed case class CNR_Signal(no: Int, samplingRate: Double, unit: String, name: String)

sealed case class CreateNewRecord(signals: scala.collection.immutable.Set[CNR_Signal])
