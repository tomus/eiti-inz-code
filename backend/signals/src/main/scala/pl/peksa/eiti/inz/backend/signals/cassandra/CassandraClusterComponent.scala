package pl.peksa.eiti.inz.backend.signals.cassandra

import com.datastax.driver.core.Cluster

trait CassandraClusterComponent {
  val cassandraCluster: Cluster
}
