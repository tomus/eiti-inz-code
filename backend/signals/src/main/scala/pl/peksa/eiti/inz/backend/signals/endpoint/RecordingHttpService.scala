package pl.peksa.eiti.inz.backend.signals.endpoint

import java.io.StringWriter
import java.time.ZonedDateTime
import java.util.Scanner
import java.util.regex.Pattern

import akka.pattern.ask
import akka.util.Timeout
import com.github.tototoshi.csv.{CSVReader, CSVWriter}
import com.typesafe.scalalogging.StrictLogging
import pl.peksa.eiti.inz.backend.signals.commands.{AddSamples, CreateNewRecord, SampleData}
import pl.peksa.eiti.inz.backend.signals.processing.actors.{SampleStreamingToClientActor, SignalActor}
import pl.peksa.eiti.inz.backend.signals.queries.{QueryRecordMetadata, QueryRecordSamples}
import pl.peksa.eiti.inz.backend.signals.responses._
import spray.http.HttpHeaders.Location
import spray.http._
import spray.httpx.marshalling.Marshaller
import spray.httpx.unmarshalling.Unmarshaller
import spray.routing._
import scala.collection.JavaConversions._
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.util.{Failure, Success}

trait RecordingHttpService extends HttpService with JsonSupport with StrictLogging {

  def createPartialSignalWithSamples(samples: SampleData): Seq[SignalWithSamples] = {
    //todo refactor this out
    def signalNameToNumber(name: String): Int = {
      val hashIndex = name.indexOf('#')
      if (hashIndex == -1) name.toInt else name.substring(0, hashIndex).toInt
    }
    (0 until samples.signals.length).map { i =>
      val signalName: String = samples.signals(i)
      val signalNo = signalNameToNumber(signalName)
      val samplesForSignal = new SignalPart(samples.dateTime, new Frequency(360.0), samples.samples.map {
        _(i)
      })
      new SignalWithSamples(signalNo, "unknown", signalName, samplesForSignal)
    }
  }


  def recordRepository = actorRefFactory.actorSelection("/user/recordRepository")

  def recordManager = actorRefFactory.actorSelection("/user/recordManager")

  implicit val timeout: Timeout = 15.seconds

  implicit def executionContext: ExecutionContextExecutor = actorRefFactory.dispatcher

  implicit val sampleDataUnmarshaller =
    Unmarshaller[SampleData](ContentTypeRange.*) {
      case HttpEntity.NonEmpty(contentType, data) =>
        val scanner = new Scanner(data.asString)
        scanner.useDelimiter(Pattern.compile("\\n"))
        val reader = scanner.map(_.split(','))
        val date = ZonedDateTime.parse(reader.next().head)
        val signals = reader.next()
        val samples = reader.map { line: Array[String] => line.map { s: String => s.toDouble}.toArray}.toList
        new SampleData(date, signals, samples)
    }

  val `application/vnd.pl.peksa.eiti.inz.samples` = MediaTypes.register(MediaType.custom("application/vnd.samples"))

  implicit val recordSamplesMarshaller =
    Marshaller.of[RecordSamples](`application/vnd.pl.peksa.eiti.inz.samples`) { (value, contentType, ctx) =>
      def readLineValues[A](iterators: Seq[Iterator[A]]): Seq[Option[A]] = {
        iterators.map(iterator => if (iterator.hasNext) Some(iterator.next()) else None)
      }
      def allAreNone(options: Option[_]*): Boolean = options.filterNot(_.isEmpty).isEmpty

      val stringWriter = new StringWriter()
      val csvWriter = CSVWriter.open(stringWriter)
      val signals = value.signals.toSeq
      val samples = signals.map(_.samples.values)
      csvWriter.writeRow(Seq(value.recordId))
      csvWriter.writeRow(signals.map { signal => s"${signal.no}#${signal.name}[${signal.unit}]@${signal.samples.samplingRate.value}"})

      var lineValues: Seq[Option[Double]] = readLineValues(samples)
      while (!allAreNone(lineValues: _ *)) {
        csvWriter.writeRow(lineValues.map(_.getOrElse("")))
        lineValues = readLineValues(samples)
      }
      ctx.marshalTo(HttpEntity(contentType, stringWriter.getBuffer.toString))
    }

  val routes = pathPrefix("record") {
    pathEndOrSingleSlash {
      post {
        entity(as[CreateNewRecord]) { command =>
          ctx =>
            val future = (recordRepository ? command).mapTo[NewRecordCreated]
            future.onComplete {
              case Success(response) =>
                ctx
                  .withHttpResponseHeadersMapped(_ ++ Seq(Location(Uri(s"/record/${response.recordId}"))))
                  .complete((StatusCodes.Created, response))
              case Failure(f) =>
                logger.error("Unable to save record!", f)
                ctx
                  .failWith(f)
            }
        }
      }
    } ~ pathPrefix(JavaUUID) { recordId: java.util.UUID =>
      pathEndOrSingleSlash {
        get {
          complete {
            (recordRepository ? QueryRecordMetadata(recordId)).mapTo[CreateNewRecord]
          }
        } ~ put {
          complete {
            "update recording metadata"
          }
        }
      } ~ pathPrefix("samples") {
        pathEndOrSingleSlash {
          post {
            entity(as[SampleData]) { sampleData =>
              complete {
                val command = new AddSamples(recordId, sampleData)
                val message = s"Added ${command.sampleData.samples.size} samples for each channel (${command.sampleData.signals.size}) for $recordId."
                createPartialSignalWithSamples(sampleData).foreach {
                  recordManager ! SignalActor.NewSignalPartReceived(recordId, _)
                }

                (recordRepository ? command).map(_ => "message" -> message) //todo move saving to database to actor
              }
            }
          } ~ get {
            parameters('offset.?, 'duration.?) { (offsetStr: Option[String], durationStr: Option[String]) =>
              complete {
                val offset: FiniteDuration = offsetStr.map(Duration(_).asInstanceOf[FiniteDuration]).getOrElse(0 milliseconds)
                val duration: Duration = durationStr.map(Duration(_)).getOrElse(Duration.Inf)
                val query = QueryRecordSamples(recordId, duration, offset)
                (recordRepository ? query).mapTo[RecordSamples]
              }
            }
          }
        } ~ pathPrefix("stream") {
          pathEndOrSingleSlash {
            get { ctx =>
              actorRefFactory.actorOf(SampleStreamingToClientActor.props(recordId, ctx, Set(1, 2)))
            }
          }
        }

      }
    }
  }
}
