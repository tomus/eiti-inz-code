package pl.peksa.eiti.inz.backend.signals.processing.filters

abstract class RecursiveFilter[T](override val previousInputMemorySize: Int, val previousOutputMemorySize: Int)
  extends Processor[T, T] with InputMemory[T]
  with OutputMemory[T] {

  val output: (SampleMemory, SampleMemory) => T


  override def apply(source: T): T = {
    memorizeInput(source)
    val outputValue = output(inputMemory, outputMemory)
    memorizeOutput(outputValue)
    outputValue
  }

}
