package pl.peksa.eiti.inz.backend.signals.cassandra

import com.google.common.util.concurrent.{FutureCallback, Futures, ListenableFuture}

import scala.concurrent.{Promise, Future}

object FutureUtils {

  implicit class RichListenableFuture[T](lf: ListenableFuture[T]) {
    def toPromise: Future[T] = {
      val p = Promise[T]()
      Futures.addCallback(lf, new FutureCallback[T] {
        def onFailure(t: Throwable): Unit = p failure t

        def onSuccess(result: T): Unit = p success result
      })
      p.future
    }
  }

}
