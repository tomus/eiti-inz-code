package pl.peksa.eiti.inz.backend.signals.processing.actors

import akka.actor.{ActorRefFactory, Actor, ActorRef, Props}
import com.typesafe.scalalogging.StrictLogging
import pl.peksa.eiti.inz.backend.signals.Identifiable

class IdBasedDispatchingActor[T](val baseActorName: String, childPropsMaker: T => Props) extends Actor with StrictLogging {

  override def receive: Receive = {
    case msg: Identifiable[T] => forwardToChild(msg)
  }

  def forwardToChild(msg: Identifiable[T]): Unit = {
    val destination = actorForId(msg.id)
    logger.trace(s"Forwarding $msg to ${msg.id} ($destination)")
    destination forward msg
  }

  def actorForId(id: T): ActorRef = {
    def createChild: ActorRef = {
      logger.trace(s"Creating child for $id")
      context.actorOf(childPropsMaker(id), idToChildName(id))
    }
    context.child(idToChildName(id)).getOrElse(createChild)
  }

  private[this] def idToChildName(id: T): String = {
    s"$baseActorName-$id"
  }
}

object IdBasedDispatchingActor {
  def props[T](baseActorName: String, childPropsMaker: T => Props): Props = Props(classOf[IdBasedDispatchingActor[T]], baseActorName, childPropsMaker)
}
