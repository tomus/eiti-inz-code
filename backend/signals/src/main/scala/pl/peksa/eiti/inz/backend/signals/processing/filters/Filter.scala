package pl.peksa.eiti.inz.backend.signals.processing.filters

abstract class Filter[T](val previousInputMemorySize: Int) extends Processor[T, T] with InputMemory[T] {

  val output: SampleMemory => T

  override def apply(source: T): T = {
    memorizeInput(source)
    output(inputMemory)
  }

}

