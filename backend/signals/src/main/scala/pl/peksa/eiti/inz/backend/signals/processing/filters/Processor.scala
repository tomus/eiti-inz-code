package pl.peksa.eiti.inz.backend.signals.processing.filters

abstract class Processor[S,D] extends (S => D) {
}

object Processor {
  type SampleOffset = Int
}
