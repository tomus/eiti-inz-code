package pl.peksa.eiti.inz.backend.signals.processing.actors

import java.util.UUID

import akka.actor.{Actor, Cancellable, Props}
import com.typesafe.scalalogging.StrictLogging
import org.json4s.native.Serialization._
import pl.peksa.eiti.inz.backend.signals.processing.actors.SignalActor.NewSignalPartReceived
import pl.peksa.eiti.inz.backend.signals.responses.SignalWithSamples
import spray.http._
import spray.routing.RequestContext

import scala.concurrent.duration._

class SampleStreamingToClientActor(val recordId: UUID, val signalsToStream: Set[Int], val clientRequest: RequestContext) extends Actor with StrictLogging {

  import pl.peksa.eiti.inz.backend.signals.processing.actors.SampleStreamingToClientActor._

  val `text/event-stream` = MediaTypes.register(MediaType.custom("text/event-stream"))
  var tickCancellable: Option[Cancellable] = None

  def recordManager = context.actorSelection("/user/recordManager")

  import context.dispatcher

  private[this] def subscribeSignals(): Unit = {
    signalsToStream.foreach { signalNo: Int =>
      recordManager ! SignalActor.SignalSubscription(recordId, signalNo)
    }
  }

  override def preStart(): Unit = {
    tickCancellable = Some(context.system.scheduler.schedule(0.seconds, 15.seconds, self, Tick))
    val responseStart = HttpResponse(entity = HttpEntity(`text/event-stream`, sseMessage("hello")))
    clientRequest.responder ! ChunkedResponseStart(responseStart)
    subscribeSignals()
  }

  override def postStop(): Unit = {
    if (tickCancellable.isDefined) tickCancellable.get.cancel()
  }

  override def receive: Receive = {
    case NewSignalPartReceived(_, signalPart) =>
      logger.trace(s"MessageChunk to $clientRequest")
      clientRequest.responder ! MessageChunk(signalPart)
    case Tick =>
      logger.trace(s"Sending tick to $clientRequest")
      clientRequest.responder ! MessageChunk(sseMessage("tick"))
    case akka.io.Tcp.PeerClosed =>
      logger.trace(s"PeerClosed $clientRequest, closing")
      context.stop(self)
    case x =>
      logger.error(s"Not handled $x")
  }
}

object SampleStreamingToClientActor {

  import pl.peksa.eiti.inz.backend.signals.endpoint.JsonSupport._

  def props(recordId: UUID, request: RequestContext, signals: Set[Int]): Props = Props(classOf[SampleStreamingToClientActor], recordId, signals, request)

  def sseMessage(event: String, data: String = ""): String = {
    assert(!data.contains("\n"))
    s"event: $event\ndata: $data\n\n"
  }

  implicit def signalWithSamplesMarshaller(signal: SignalWithSamples): HttpData = {
    val data = write(signal)
    val responseString = sseMessage("samples", data)
    HttpData(responseString)
  }

  case object Tick

}