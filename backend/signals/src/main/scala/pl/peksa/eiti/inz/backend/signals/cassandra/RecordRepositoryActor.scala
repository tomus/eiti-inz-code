package pl.peksa.eiti.inz.backend.signals.cassandra

import akka.actor.{Actor, Props}
import akka.pattern._
import com.datastax.driver.core._
import pl.peksa.eiti.inz.backend.signals.commands.{AddSamples, CreateNewRecord}
import pl.peksa.eiti.inz.backend.signals.queries.{QueryRecordSamples, QueryRecordMetadata}

class RecordRepositoryActor(cluster: Cluster) extends Actor with CassandraRecordRepositoryComponent with CassandraClusterComponent {

  override val cassandraCluster: Cluster = cluster

  import scala.concurrent.ExecutionContext.Implicits.global

  override def receive: Receive = {
    case command: CreateNewRecord => repository.store(command) pipeTo sender()
    case QueryRecordMetadata(recordId) => repository.readMetadata(recordId) pipeTo sender()
    case command: AddSamples => repository.storeSamples(command.recordId, command.sampleData) pipeTo sender()
    case query: QueryRecordSamples => repository.querySamples(query.recordId, query.duration, query.offset) pipeTo sender()
  }
}

object RecordRepositoryActor {
  def props(cluster: Cluster) = {
    Props(classOf[RecordRepositoryActor], cluster)
  }
}