package pl.peksa.eiti.inz.backend.signals.endpoint

import akka.actor.{Actor, ActorLogging}
import com.typesafe.scalalogging.StrictLogging
import org.apache.commons.lang3.exception.ExceptionUtils
import spray.http._
import spray.routing._

class SignalsEndpointActor extends Actor with SignalsEndpoint with ActorLogging {

  override def actorRefFactory = context

  def receive = runRoute(endpoint)

}

trait SignalsEndpoint extends HttpService with StrictLogging {

  val recording = new RecordingHttpService() {
    def actorRefFactory = SignalsEndpoint.this.actorRefFactory
  }

  val exceptionHandler = ExceptionHandler {
    case e: Throwable =>
      println("#"+e)
      requestUri { uri =>
        logger.error(s"Request to $uri could not be handled normally", e)
        complete(HttpResponse(StatusCodes.InternalServerError, HttpEntity(s"Request to $uri could not be handled normally\n${ExceptionUtils.getStackTrace(e)}")))
      }
  }

  val endpoint = handleExceptions(exceptionHandler) {
    respondWithSingletonHeader(HttpHeaders.`Access-Control-Allow-Origin`(AllOrigins)) {
      recording.routes
    }
  }
}