package pl.peksa.eiti.inz.backend.signals.endpoint

import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.ext.JavaTypesSerializers
import pl.peksa.eiti.inz.backend.signals.responses.SignalPart

trait JsonSupport extends spray.httpx.Json4sSupport {
  implicit def json4sFormats: Formats = DefaultFormats ++ JavaTypesSerializers.all + SamplesSerializer
}

object JsonSupport extends JsonSupport

case object SamplesSerializer extends CustomSerializer[SignalPart](format => ( {
  case jv: JValue => ???
}, {
  case x: SignalPart => ("startTime" -> x.startTime.toString) ~
    ("samplingRate" -> JDouble(x.samplingRate.value)) ~
    ("values" -> x.values.toStream)
}
  )
)
