package pl.peksa.eiti.inz.backend.signals.processing.filters.utils

class FixedSizeMemory[T](size: Int) {

  private[this] var values = Seq.empty[T]

  def add(value: T): Unit = {
    values = (value +: values).take(size)
  }

  def get(index: Int): T = {
    values(index)
  }

  def getAll = values

}

object FixedSizeMemory {
  sealed case class InitialValue[T](value: T)

  def createInitialized[T](size: Int, initialValue: InitialValue[T]) = {
    val memory: FixedSizeMemory[T] = new FixedSizeMemory[T](size)
    (0 until size).foreach(i => {
      memory.add(initialValue.value)
    })
    memory
  }

}