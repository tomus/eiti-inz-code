package pl.peksa.eiti.inz.backend.signals.processing.filters

import pl.peksa.eiti.inz.backend.signals.processing.filters.utils.FixedSizeMemory
import pl.peksa.eiti.inz.backend.signals.processing.filters.utils.FixedSizeMemory.InitialValue
import pl.peksa.eiti.inz.backend.signals.responses.Frequency

import scala.collection.AbstractIterator
import scala.concurrent.duration._

object RrInterval {

  class IntervalCalculator extends Processor[Double, Int] {
    var counter = 0

    override def apply(sampleValue: Double): Int = {
      val isRPoint = sampleValue == 1
      if (isRPoint) {
        counter = 0
        -1
      } else {
        val c = counter
        counter += 1
        c
      }
    }
  }

  val a: Double = 0.9
  val b: Double = 0.9
  val c: Double = 1.5

  def windowToCategory(rr1: Int, rr2: Int, rr3: Int): Int = {
    if (rr2 < 0.6.seconds && rr2 < rr3) {
      5
    } else if (rr2 < a * rr1 && rr1 < b * rr3) {
      if (rr2 + rr3 < 2 * rr1) {
        2
      } else {
        3
      }
    } else if (rr2 > c * rr1) {
      1 //should be 4 TODO
    } else {
      1
    }
  }

  class ArrhythmiaDetector extends Processor[Int, Int] {
    val avgThreshold = (0.8, 1.25)
    val statistics = new FixedSizeMemory[Int](15)
    val buffer = FixedSizeMemory.createInitialized[Int](3, InitialValue(0))
    var lastInterval = -1

    def isLastOutstanding: Boolean = {
      if (statistics.getAll.size < 10) {
        false
      } else {
        val last::previous = statistics.getAll
        val average = previous.sum / previous.size
        val isAbnormal = !Range((avgThreshold._1 * average).toInt, (avgThreshold._2 * average).toInt).contains(last)
        isAbnormal
      }
    }

    override def apply(value: Int): Int = {
      val intervalIsFinished = value < 0
      if (intervalIsFinished) {
        val finishedIntervalLength = lastInterval
        lastInterval = -1
        buffer.add(finishedIntervalLength)
        statistics.add(finishedIntervalLength)
      } else {
        lastInterval = value
      }
      val category = windowToCategory(buffer.get(0), buffer.get(1), buffer.get(2)) - 1
      val statisticalResult = if (isLastOutstanding) 33 else 0
      val result = if(category>0) category else statisticalResult
      result
    }
  }

  val frequency = new Frequency(360.0)

  implicit def durationToSamples(duration: FiniteDuration): Int = ((frequency.value * duration.toMillis).toLong / 1000).toInt

}
