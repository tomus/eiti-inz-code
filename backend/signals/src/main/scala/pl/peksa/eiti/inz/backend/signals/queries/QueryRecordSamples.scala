package pl.peksa.eiti.inz.backend.signals.queries

import java.util.UUID

import scala.concurrent.duration.{FiniteDuration, Duration}

sealed case class QueryRecordSamples(recordId: UUID, duration: Duration, offset: FiniteDuration)
