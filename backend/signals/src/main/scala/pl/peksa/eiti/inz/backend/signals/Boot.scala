package pl.peksa.eiti.inz.backend.signals

import java.util.UUID

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import com.datastax.driver.core.Cluster
import com.typesafe.config.ConfigFactory
import pl.peksa.eiti.inz.backend.signals.cassandra.{ConfigCassandraCluster, RecordRepositoryActor}
import pl.peksa.eiti.inz.backend.signals.endpoint.SignalsEndpointActor
import pl.peksa.eiti.inz.backend.signals.processing.actors.{IdBasedDispatchingActor, RecordActor}
import spray.can.Http

import scala.concurrent.duration._
import scala.collection.JavaConversions._

object Boot extends App with ConfigCassandraCluster {

  implicit val timeout = Timeout(5.seconds)
  implicit val system = ActorSystem("signals-akka")
  val recordRepositoryActor = system.actorOf(RecordRepositoryActor.props(cluster), "recordRepository")
  val recordManagerActor = system.actorOf(IdBasedDispatchingActor.props[UUID]("record", _ => RecordActor.props), "recordManager")
  val endpointActor = system.actorOf(Props[SignalsEndpointActor], "signals-endpoint")

  IO(Http) ? Http.Bind(endpointActor, interface = config.getString("arrhytmio.endpoint.interface"), port = config.getInt("arrhytmio.endpoint.port"))

}
