package pl.peksa.eiti.inz.backend.signals.processing.actors

import java.util.UUID

import akka.actor.{Actor, ActorRef, Props}
import com.typesafe.scalalogging.StrictLogging
import pl.peksa.eiti.inz.backend.signals.processing.actors.SignalActor.{NewSignalPartReceived, SignalSubscription}
import pl.peksa.eiti.inz.backend.signals.responses.{SignalPart, SignalWithSamples}

class SignalActor extends Actor with StrictLogging {
  private[this] var subscribers = List.empty[ActorRef]

  override def receive: Receive = {
    case SignalSubscription(recordId, signalId) =>
      logger.info(s"${sender()} subscribed to $recordId, $signalId")
      subscribers = sender() +: subscribers
    case msg: NewSignalPartReceived =>
      val samples = msg.signal.samples
      subscribers.foreach { subscriber =>
        val samplesToSend = new SignalPart(samples.startTime, samples.samplingRate, samples.valueSource)
        val msgToSent = NewSignalPartReceived(msg.recordId, msg.signal.copy(samples = samplesToSend))
        subscriber ! msgToSent
      }
    case x =>
      logger.error(s"Not handled $x")
  }
}

object SignalActor {
  def props: Props = Props(classOf[SignalActor])

  sealed trait SignalIdAwareMessage {
    val signalNo: Int
  }

  sealed case class SignalSubscription(override val recordId: UUID, signalNo: Int) extends RecordActor.RecordIdAwareMessage(recordId) with SignalIdAwareMessage

  sealed case class NewSignalPartReceived(override val recordId: UUID, signal: SignalWithSamples) extends RecordActor.RecordIdAwareMessage(recordId) with SignalIdAwareMessage {
    override val signalNo: Int = signal.no
  }

}
