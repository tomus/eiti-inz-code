package pl.peksa.eiti.inz.backend.signals.processing.filters

import pl.peksa.eiti.inz.backend.signals.processing.filters.utils.FixedSizeMemory

class MovingWindowIntegrator(val xSource: Iterator[Double], val windowSize: Int) extends Iterator[Double] {
  private[this] val xBufferSize = windowSize
  private[this] val xCurrentAndPreviousBuffer = new FixedSizeMemory[Double](xBufferSize)
  (0 until xBufferSize).foreach(i => {
    xCurrentAndPreviousBuffer.add(0)
  })

  override def next(): Double = {
    xCurrentAndPreviousBuffer.add(xSource.next())
    xCurrentAndPreviousBuffer.getAll.sum / windowSize
  }

  override def hasNext: Boolean = xSource.hasNext
}
