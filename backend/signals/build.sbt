import sbtassembly.AssemblyPlugin._
import sbtdocker.Plugin.DockerKeys._
import sbtdocker.mutable.Dockerfile

libraryDependencies ++= {
  val akkaV = "2.3.6"
  val sprayV = "1.3.2"
  Seq(
    "io.spray" %% "spray-can" % sprayV,
    "io.spray" %% "spray-routing" % sprayV,
    "io.spray" %% "spray-testkit" % sprayV % "test",
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-testkit" % akkaV % "test",
    "com.typesafe.akka" %% "akka-slf4j" % akkaV,
    "org.scalatest" %% "scalatest" % "2.2.2" % "test",
    "ch.qos.logback" % "logback-classic" % "1.1.2",
    "com.datastax.cassandra" % "cassandra-driver-core" % "2.1.2",
    "org.json4s" %% "json4s-native" % "3.2.11",
    "org.json4s" %% "json4s-ext" % "3.2.11",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
    "org.apache.commons" % "commons-lang3" % "3.3.2",
    "com.github.tototoshi" %% "scala-csv" % "1.0.0",
    "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.1.3" % "test",
    "io.gatling" % "gatling-test-framework" % "2.1.3" % "test"
  )
}


dockerSettings

assemblySettings

test in assembly := {}

// Make the docker task depend on the assembly task, which generates a fat JAR file
docker <<= (docker dependsOn assembly)

dockerfile in docker := {
  val artifact = (outputPath in assembly).value
  val artifactTargetPath = s"/app/${artifact.name}"
  new Dockerfile {
    from("dockerfile/java:oracle-java8")
    add(artifact, artifactTargetPath)
    entryPoint("java", "-jar", artifactTargetPath)
  }
}
