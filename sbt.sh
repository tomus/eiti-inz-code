#!/bin/sh
 
if [ -z "$SBT_VERSION" ]
then
  SBT_VERSION=0.13.5
fi
 
JAR=$HOME/.sbt/sbt-launch-$SBT_VERSION.jar
 
mkdir -p $HOME/.sbt
mkdir -p $HOME/.sbt/boot
 
if [ ! -r $JAR ]
then
  URL="http://repo.typesafe.com/typesafe/ivy-releases/org.scala-sbt/sbt-launch/$SBT_VERSION/sbt-launch.jar"
  echo "Downloading sbt $SBT_VERSION from $URL"
  wget -q -O "$JAR" $URL
 
  if [ ! -r $JAR ]
  then
    echo "Unable to download file."
  fi
fi

if [ -z $JAVA_HOME ]
then
  echo "No JAVA_HOME" >&2
  exit 1
fi

exec $JAVA_HOME/bin/java \
    $JVM_ARGS \
    -d64 \
    -noverify \
    -Dfile.encoding=UTF8 \
    -Dsbt.boot.directory=$HOME/.sbt/boot \
    -Xmx1024M -Xss1M -XX:MaxPermSize=256m \
    -XX:+CMSClassUnloadingEnabled \
    -jar $JAR "$@"

