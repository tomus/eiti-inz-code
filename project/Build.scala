import sbt.Keys._
import sbt._
import com.typesafe.sbt.web.SbtWeb

object Build extends sbt.Build {

  val baseSettings = Seq(
    scalaVersion := "2.11.2",
    scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")
  )

  lazy val root = Project(id = "root", base = file(".")).aggregate(backend).settings(baseSettings: _*)
  lazy val backend = Project(id = "backend", base = file("backend")).aggregate(signals).settings(baseSettings: _*)
  lazy val signals = Project(id = "backend_signals", base = file("backend/signals")).settings(baseSettings: _*)
  lazy val frontend = Project(id = "frontend", base = file("frontend")).enablePlugins(SbtWeb)
}
